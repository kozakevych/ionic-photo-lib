import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavoritesPhotosPage } from './favorites-photos.page';
import { ImageGridViewComponentModule } from '../image-grid-view/image-grid-view.module';
import { PhotoLoaderService } from '../services/photo-loader.service';

import { FavoritesPhotosPageRoutingModule } from './favorites-photos-routing.module';
import { StorageService } from '../services/storage.service';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    ImageGridViewComponentModule,
    FavoritesPhotosPageRoutingModule
  ],
  declarations: [FavoritesPhotosPage],
  providers: [PhotoLoaderService, StorageService]
})
export class FavoritesPhotosPageModule {}
