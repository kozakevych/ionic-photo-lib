import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StorageService } from './../services/storage.service';
import { ImageGridViewComponentModule } from '../image-grid-view/image-grid-view.module';
import { StorageServiceMock } from './../helpers/storage-service-mock';

import { FavoritesPhotosPage } from './favorites-photos.page';

describe('FavoritesPhotosPage', () => {
  let component: FavoritesPhotosPage;
  let fixture: ComponentFixture<FavoritesPhotosPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FavoritesPhotosPage],
      imports: [IonicModule.forRoot(), ImageGridViewComponentModule],
      providers: [
        {
          provide: StorageService,
          useValue: StorageServiceMock
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(FavoritesPhotosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ionViewWillEnter', () => {
    it('should call getFavoritesPhotoList', () => {
      spyOn(component, 'getFavoritesPhotoList');
      component.ionViewWillEnter();
      expect(component.getFavoritesPhotoList).toHaveBeenCalled();
    });
  });

  describe('getFavoritesPhotoList', () => {
    it('should set photoList value', fakeAsync(() => {
      component.getFavoritesPhotoList();
      tick();
      expect(component.photoList as any).toEqual([1]);
    }));
  });

  describe('openPhotoDetail', () => {
    it('should call router with photo id param', () => {
      spyOn(component.router, 'navigate');
      const expectedArg = ['/photos', '1'];
      component.openPhotoDetail({id: '1'} as any);
      expect(component.router.navigate).toHaveBeenCalledWith(expectedArg);
    });
  });
});
