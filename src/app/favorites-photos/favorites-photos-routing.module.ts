import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FavoritesPhotosPage } from './favorites-photos.page';

const routes: Routes = [
  {
    path: '',
    component: FavoritesPhotosPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FavoritesPhotosPageRoutingModule {}
