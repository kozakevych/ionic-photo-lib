import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ViewWillEnter } from '@ionic/angular';

import { StorageService } from './../services/storage.service';
import { Photo } from './../models/photo.model';

@Component({
  selector: 'app-favorites-photos',
  templateUrl: 'favorites-photos.page.html',
  styleUrls: ['favorites-photos.page.scss']
})
export class FavoritesPhotosPage implements ViewWillEnter {

  photoList: Photo[] = [];
  constructor(public storageService: StorageService, public router: Router) {}

  ionViewWillEnter() {
    this.getFavoritesPhotoList();
  }

  getFavoritesPhotoList() {
    this.storageService.get('favorites')?.then((data: Photo[]) => {
      this.photoList = data;
    })
  }

  openPhotoDetail(photo: Photo) {
    this.router.navigate(['/photos', photo.id]);
  }
}
