import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { StorageServiceMock } from './../helpers/storage-service-mock';
import { StorageService } from './../services/storage.service';
import { ImageGridViewComponentModule } from '../image-grid-view/image-grid-view.module';

import { PhotoStreamPage } from './photo-stream.page';

describe('PhotoStreamPage', () => {
  let component: PhotoStreamPage;
  let fixture: ComponentFixture<PhotoStreamPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PhotoStreamPage],
      imports: [
        IonicModule.forRoot(),
        ImageGridViewComponentModule,
        HttpClientModule,
        MatSnackBarModule
      ],
      providers: [
        {
          provide: StorageService,
          useValue: StorageServiceMock
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PhotoStreamPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('logScrolling', () => {
    it('should call debounceLoadMore', () => {
      spyOn(component, 'debounceLoadMore');
      const eventMock = {
        srcElement: {
          querySelector: (_: any) => {return {offsetHeight: 10}},
          scrollHeight: 5
        },
        detail: {
          scrollTop: 5
        }
      }
      component.logScrolling(eventMock);
      expect(component.debounceLoadMore).toHaveBeenCalled();
    });
  });

  describe('debounceLoadMore', () => {
    it('should call handleLoadMore only once', fakeAsync(() => {
      spyOn(component, 'handleLoadMore');
      component.debounceLoadMore();
      component.debounceLoadMore();
      component.debounceLoadMore();
      tick(550);
      expect(component.handleLoadMore).toHaveBeenCalledTimes(1);
    }));
  });

  describe('handlePhotoClick', () => {
    it('should call storage service set', fakeAsync(() => {
      spyOn(component.storageService, 'get').and.returnValue(Promise.resolve([]));
      spyOn(component.storageService, 'set');
      const snackbarMock = ((test: any) => {return}) as any;
      spyOn(component.snackBar, 'open').and.callFake(snackbarMock);
      component.handlePhotoClick({id: '1'} as any);
      tick();
      expect(component.storageService.set).toHaveBeenCalled();
    }));
    it('should call snackbar open if item is already in the list', fakeAsync(() => {
      spyOn(component.storageService, 'get').and.returnValue(Promise.resolve([{ id: '1' }]));
      spyOn(component.storageService, 'set');
      const snackbarMock = ((test: any) => {return}) as any;
      spyOn(component.snackBar, 'open').and.callFake(snackbarMock);
      component.handlePhotoClick({id: '1'} as any);
      tick();
      expect(component.snackBar.open).toHaveBeenCalled();
    }));
  });
});
