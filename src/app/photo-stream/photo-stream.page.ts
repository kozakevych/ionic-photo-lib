import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnDestroy } from '@angular/core';
import { delay, Subject, switchMap, takeUntil } from 'rxjs';

import { StorageService } from './../services/storage.service';
import { PhotoLoaderService } from '../services/photo-loader.service';
import { Photo } from './../models/photo.model';

@Component({
  selector: 'app-photo-stream-page',
  templateUrl: 'photo-stream.page.html',
  styleUrls: ['photo-stream.page.scss']
})
export class PhotoStreamPage implements OnDestroy {
  public imagesList: Photo[] = [];
  public isLoading: boolean = false;
  private loadPhotos$ = new Subject<any>();
  private destroy$: Subject<boolean> = new Subject<boolean>();
  private debounceTimeout: any;
  private page = 1;

  constructor(
    public photoLoaderService: PhotoLoaderService,
    public storageService: StorageService,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.loadPhotos$.pipe(
      switchMap(({count, page}) => {
        this.isLoading = true;
        return this.photoLoaderService.getPhoto(count, page)
      }),
      delay(400),
      takeUntil(this.destroy$)
    ).subscribe((data: Photo[]) => {
      this.isLoading = false;
      this.imagesList.push(...data);
    });
    this.initImageLoad();
  }

  initImageLoad() {
    this.loadPhotos$.next({count: 6, page: this.page});
  }

  handleLoadMore() {
    this.page += 1;
    this.loadPhotos$.next({count: 6, page: this.page});
  }

  logScrolling(event: any) {
    const elementOffsetHeight = event.srcElement.querySelector('#image-grid').offsetHeight;
    const scrollTop = event.detail.scrollTop;
    const scrollHeight = event.srcElement.scrollHeight;

    if (Math.abs(elementOffsetHeight - (scrollTop + scrollHeight)) < 150) {
      this.debounceLoadMore();
    }
  }

  debounceLoadMore() {
    if (this.debounceTimeout) {
      clearTimeout(this.debounceTimeout)
    }
    this.debounceTimeout = setTimeout(() => {
      this.handleLoadMore();
    }, 500);
  }

  handlePhotoClick(photo: Photo) {
    this.storageService.get('favorites')?.then((photoList: Photo[]) => {
      if (photoList && photoList.find((item: Photo) => item.id == photo.id)) {
        this.snackBar.open('You already have this photo in favorites', 'Ok', { duration: 2000 });
        return;
      }
      this.storageService.set('favorites', photoList ? [...photoList, photo] : [photo]);
      this.snackBar.open('Photo added to favorites', 'Ok', { duration: 2000 });
    })
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
