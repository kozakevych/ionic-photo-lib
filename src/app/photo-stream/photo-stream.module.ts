import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PhotoStreamPage } from './photo-stream.page';
import { ImageGridViewComponentModule } from '../image-grid-view/image-grid-view.module';
import { HttpClientModule } from '@angular/common/http';
import { PhotoLoaderService } from '../services/photo-loader.service';
import { StorageService } from './../services/storage.service';

import { MatSnackBarModule } from '@angular/material/snack-bar';

import { PhotoStreamRoutingModule } from './photo-stream-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ImageGridViewComponentModule,
    HttpClientModule,
    PhotoStreamRoutingModule,
    MatSnackBarModule
  ],
  declarations: [PhotoStreamPage],
  providers: [PhotoLoaderService, StorageService]
})
export class PhotoStreamPageModule {}
