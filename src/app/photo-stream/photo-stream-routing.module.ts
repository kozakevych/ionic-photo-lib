import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PhotoStreamPage } from './photo-stream.page';

const routes: Routes = [
  {
    path: '',
    component: PhotoStreamPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhotoStreamRoutingModule {}
