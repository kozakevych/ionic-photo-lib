import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotoDetailPage } from './photo-detail.page';
import { ImageGridViewComponentModule } from '../image-grid-view/image-grid-view.module';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { PhotoDetailPageRoutingModule } from './photo-detail-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    ImageGridViewComponentModule,
    MatSnackBarModule,
    PhotoDetailPageRoutingModule
  ],
  declarations: [PhotoDetailPage]
})
export class PhotosDetailPageModule {}
