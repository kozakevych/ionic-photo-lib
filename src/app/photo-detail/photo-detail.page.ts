import { MatSnackBar } from '@angular/material/snack-bar';
import { StorageService } from '../services/storage.service';
import { Component } from '@angular/core';
import { ViewWillEnter } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

import { Photo } from './../models/photo.model';

@Component({
  selector: 'app-photo-detail',
  templateUrl: 'photo-detail.page.html',
  styleUrls: ['photo-detail.page.scss']
})
export class PhotoDetailPage implements ViewWillEnter {
  public selectedPhoto: any;

  constructor(
    public storageService: StorageService,
    private activatedRoute: ActivatedRoute,
    public router: Router,
    public snackBar: MatSnackBar
  ) {}

  ionViewWillEnter() {
    this.getSelectedPhoto()
  }

  getSelectedPhoto() {
    const selectedId = this.activatedRoute.snapshot.paramMap.get('id');
    this.storageService.get('favorites')?.then((data: Photo[]) => {
      this.selectedPhoto = data.find((photo: Photo) => photo.id == selectedId);
      if (!this.selectedPhoto) {
        this.router.navigate(['/favorites']);
      }
    })
  }

  removeImageFromFavorites() {
    this.storageService.get('favorites')?.then((data: Photo[]) => {
      const updatedList = data.filter((item: Photo) => item.id !== this.selectedPhoto.id)
      this.storageService?.set('favorites', updatedList);
      this.snackBar.open('Photo was removed from favorites', 'Ok', {
        duration: 2000
      });
      this.router.navigate(['/favorites']);
    });
  }
}
