import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { StorageServiceMock } from './../helpers/storage-service-mock';
import { StorageService } from './../services/storage.service';
import { ImageGridViewComponentModule } from '../image-grid-view/image-grid-view.module';

import { PhotoDetailPage } from './photo-detail.page';

describe('PhotoDetailPage', () => {
  let component: PhotoDetailPage;
  let fixture: ComponentFixture<PhotoDetailPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PhotoDetailPage],
      imports: [
        IonicModule.forRoot(),
        ImageGridViewComponentModule,
        RouterTestingModule,
        MatSnackBarModule
      ],
      providers: [
        {
          provide: StorageService,
          useValue: StorageServiceMock
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PhotoDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ionViewWillEnter', () => {
    it('should call getSelectedPhoto', () => {
      spyOn(component, 'getSelectedPhoto');
      component.ionViewWillEnter();
      expect(component.getSelectedPhoto).toHaveBeenCalled();
    });
  });

  describe('getSelectedPhoto', () => {
    it('should call router navigate if selected photo not found', fakeAsync(() => {
      spyOn(component.storageService, 'get').and.returnValue(Promise.resolve([]));
      spyOn(component.router, 'navigate');
      component.getSelectedPhoto();
      tick();
      expect(component.router.navigate).toHaveBeenCalled();
    }));
  });

  describe('removeImageFromFavorites', () => {
    it('should call router navigate after updating the list', fakeAsync(() => {
      spyOn(component.storageService, 'get').and.returnValue(Promise.resolve([]));
      spyOn(component.router, 'navigate');
      const snackbarMock = ((test: any) => {return}) as any;
      spyOn(component.snackBar, 'open').and.callFake(snackbarMock);
      component.removeImageFromFavorites();
      tick();
      expect(component.router.navigate).toHaveBeenCalled();
    }));
  });
});
