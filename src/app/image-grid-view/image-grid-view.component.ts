import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Photo } from './../models/photo.model';

@Component({
  selector: 'app-image-grid',
  templateUrl: './image-grid-view.component.html',
  styleUrls: ['./image-grid-view.component.scss'],
})
export class ImageGridViewComponent {

  @Input() public imagesList: Photo[] = [];
  @Input() public isLoading: boolean = false;

  @Output() public emitPhotoClickEvent = new EventEmitter();

  onImageClick(image: Photo) {
    this.emitPhotoClickEvent.emit(image);
  }
}
