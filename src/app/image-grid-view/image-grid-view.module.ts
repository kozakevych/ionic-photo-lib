import { MatCardModule } from '@angular/material/card';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { IonicModule } from '@ionic/angular';

import { ImageGridViewComponent } from './image-grid-view.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatGridListModule,
    MatCardModule,
    MatProgressSpinnerModule
  ],
  declarations: [ImageGridViewComponent],
  exports: [ImageGridViewComponent],
})
export class ImageGridViewComponentModule {}
