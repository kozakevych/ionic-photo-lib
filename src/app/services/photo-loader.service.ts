import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

import { Photo } from './../models/photo.model';

@Injectable({
  providedIn: 'root'
})
export class PhotoLoaderService {
  // public imageUrl = 'https://picsum.photos/200/300';

  constructor(
    public httpClient: HttpClient
  ) {}

  getPhoto(count: number, page: number) {
    const imageUrl = `https://picsum.photos/v2/list?page=${page}&limit=${count}`;
    return this.httpClient.get<Photo[]>(imageUrl);
  }
}
