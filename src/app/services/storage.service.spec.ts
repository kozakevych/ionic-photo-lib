import { TestBed } from '@angular/core/testing';
import { Storage } from '@ionic/storage-angular';

import { StorageService } from './storage.service';

describe('StorageService', () => {
  let service: StorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ StorageService, Storage ]
    });
    service = TestBed.inject(StorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('set', () => {
    it('should set provided value to storage', () => {
      service.set('name', 1);
      const expectedResult = [1];
      service.get('name')?.then((data) => {
        expect(data).toEqual(expectedResult)
      });
    });
  });
});
