import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { PhotoLoaderService } from './photo-loader.service';

describe('PhotoLoaderService', () => {
  let service: PhotoLoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ]
    });
    service = TestBed.inject(PhotoLoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
