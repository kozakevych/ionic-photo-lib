import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: '',
        loadChildren: () => import('../photo-stream/photo-stream.module').then(m => m.PhotoStreamPageModule)
      },
      {
        path: 'favorites',
        loadChildren: () => import('../favorites-photos/favorites-photos.module').then(m => m.FavoritesPhotosPageModule)
      },
      {
        path: 'photos/:id',
        loadChildren: () => import('../photo-detail/photo-detail.module').then(m => m.PhotosDetailPageModule)
      },
      {
        path: '**',
        redirectTo: 'photo-stream'
      },
    ]
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
