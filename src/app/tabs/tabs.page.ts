import { ViewWillEnter } from '@ionic/angular';
import { Router } from '@angular/router';
import { Component } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements ViewWillEnter {
  public selectedTabName = '';

  constructor(private router: Router) {}

  ionViewWillEnter() {
    this.selectedTabName = this.router.url === '/favorites' ? 'favorites' : 'photostream';
  }

  selectTab(tabName: string) {
    this.selectedTabName = tabName;
  }
}
