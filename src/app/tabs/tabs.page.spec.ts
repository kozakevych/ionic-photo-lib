import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsPage } from './tabs.page';

describe('TabsPage', () => {
  let component: TabsPage;
  let fixture: ComponentFixture<TabsPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TabsPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('method: selectTab', () => {
    it('should set selectedTabName', () => {
      const photostreamTab = 'photostream';
      const favoritesTab = 'favorites';

      component.selectTab(photostreamTab);
      expect(component.selectedTabName).toBe(photostreamTab)
      component.selectTab(favoritesTab);
      expect(component.selectedTabName).toBe(favoritesTab)
    });
  });
});
