## Ionic Photo Library
- Application shows random images with infinite scroll (photostream) on first tab and list of favorite images on second. 
- To see latest changes and all commit history, please use `develop` branch;

#### To start application:
1) Clone repository;
2) Install dependencies using `npm install` command;
3) After installation complete, run `npm run start` and wait until build completes;
4) Visit `http://localhost:4200` and check the result;

--- 

##### Preview screenshot:

![Application Preview](/docs/app-preview.png "Application Preview")

---

##### Thank you for checking my repo, would appreciate your feedback and comments!
